import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ModeService {

  private modeSource = new BehaviorSubject('home');
  private indexToBeSearched: string;
  currentMode: Observable<string> = this.modeSource.asObservable();

  constructor() { }

  updateMode(newMode: string): void {
    this.modeSource.next(newMode);
  }

  updateModeTwice(newMode: string): void {
    setTimeout(() => this.updateMode('home'), 500);
    setTimeout(() => this.updateMode(newMode), 500);
  }

  getIndex(): string {
    return this.indexToBeSearched;
  }

  setIndex(newIndex: string): void {
    this.indexToBeSearched = newIndex;
  }

}
