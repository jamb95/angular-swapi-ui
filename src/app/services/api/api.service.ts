import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  private baseUrl: string = 'https://swapi.co/api/';

  constructor(
    protected httpClient: HttpClient
  ) { }

  getAllPeople(page: number): Observable<any> {
    return this.httpClient.get(this.baseUrl + 'people/?page=' + page);
  }

  getAllPlanets(page: number): Observable<any> {
    return this.httpClient.get(this.baseUrl + 'planets/?page=' + page);
  }

  getAllFilms(page: number): Observable<any> {
    return this.httpClient.get(this.baseUrl + 'films/?page=' + page);
  }

  getAllSpecies(page: number): Observable<any> {
    return this.httpClient.get(this.baseUrl + 'species/?page=' + page);
  }

  getAllVehicles(page: number): Observable<any> {
    return this.httpClient.get(this.baseUrl + 'vehicles/?page=' + page);
  }

  getAllStarships(page: number): Observable<any> {
    return this.httpClient.get(this.baseUrl + 'starships/?page=' + page);
  }
}
