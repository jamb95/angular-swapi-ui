import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModeService } from './services/mode/mode.service';
import { CacheService } from './services/cache/cache.service';
import { ApiService } from './services/api/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {

  modeSubscription: Subscription;
  currentMode: string;

  constructor(
    protected modeService: ModeService,
    protected cacheService: CacheService,
    protected apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.modeSubscription = this.modeService.currentMode.subscribe(mode => this.currentMode = mode);
  }

  ngOnDestroy(): void {
    this.modeSubscription.unsubscribe();
  }

}
