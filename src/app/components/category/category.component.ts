import { Component, OnInit, Input, Inject, Output, EventEmitter } from '@angular/core';
import { ModeService } from 'src/app/services/mode/mode.service';
import { ApiService } from 'src/app/services/api/api.service';
import { MatDropdown } from 'src/app/models/mat-dropdown';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  cardInfoTitle: string;
  cardInfoData: any;
}

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})

export class CategoryComponent implements OnInit {

  @Input() mode: string;

  modeHtml: string;
  categoryData: any;
  cachedCategoryEntries: any[] = [];
  realCategoryEntries: any[] = [];
  selectedEntry: any;
  dropdownOptions: MatDropdown[] = [
    {
      value: "index",
      viewValue: "index"
    }
  ];
  dropdownSelection: string = "index";
  cardFields: any[] = [];
  cardButtonsData: any[] = [];
  searchInput: string;
  currentPage: number = 1;
  isLastPage: boolean;

  constructor(
    protected modeService: ModeService,
    protected apiService: ApiService,
    protected matDialog: MatDialog
  ) {

  }

  ngOnInit(): void {
    this.modeHtml = this.capitalizeFLetter(this.mode);

    const cachedIndex = this.modeService.getIndex();
    if (cachedIndex) {
      const page = Math.floor(Number.parseInt(cachedIndex) / 10) + 1;
      this.initData(this.mode, page);
    } else {
      this.initData(this.mode, this.currentPage);
    }

  }

  private initData(mode: string, page: number): void {
    switch (mode) {
      case 'people':
        this.apiService.getAllPeople(page)
          .subscribe(
            peopleData => {
              this.categoryData = peopleData;
              this.cachedCategoryEntries = peopleData.results;
              this.populateInitialSearchInput(peopleData.results.length);
            },
            err => { }
          );
        break;
      case 'planets':
        this.apiService.getAllPlanets(page)
          .subscribe(
            planetsData => {
              this.categoryData = planetsData;
              this.cachedCategoryEntries = planetsData.results;
              this.populateInitialSearchInput(planetsData.results.length);
            },
            err => { }
          );
        break;
      case 'films':
        this.apiService.getAllFilms(page)
          .subscribe(
            filmsData => {
              this.categoryData = filmsData;
              this.cachedCategoryEntries = filmsData.results;
              this.populateInitialSearchInput(filmsData.results.length);
            },
            err => { }
          );
        break;
      case 'species':
        this.apiService.getAllSpecies(page)
          .subscribe(
            speciesData => {
              this.categoryData = speciesData;
              this.cachedCategoryEntries = speciesData.results;
              this.populateInitialSearchInput(speciesData.results.length);
            },
            err => { }
          );
        break;
      case 'vehicles':
        this.apiService.getAllVehicles(page)
          .subscribe(
            vehiclesData => {
              this.categoryData = vehiclesData;
              this.cachedCategoryEntries = vehiclesData.results;
              this.populateInitialSearchInput(vehiclesData.results.length);
            },
            err => { }
          );
        break;
      case 'starships':
        this.apiService.getAllStarships(page)
          .subscribe(
            starshipsData => {
              this.categoryData = starshipsData;
              this.cachedCategoryEntries = starshipsData.results;
              this.populateInitialSearchInput(starshipsData.results.length);
            },
            err => { }
          );
        break;
    }
  }

  displayDetailsForEntry(entry: any): void {
    this.cardFields.length = 0;
    this.cardButtonsData.length = 0;

    this.selectedEntry = entry;

    for (let [key, value] of Object.entries(entry)) {
      switch (this.mode) {
        case 'people':
          this.populatePeopleCard(key, value);
          break;
        case 'planets':
          this.populatePlanetsCard(key, value);
          break;
        case 'films':
          this.populateFilmsCard(key, value);
          break;
        case 'species':
          this.populateSpeciesCard(key, value);
          break;
        case 'vehicles':
          this.populateVehiclesCard(key, value);
          break;
        case 'starships':
          this.populateStarshipsCard(key, value);
          break;
      }
    }

  }

  filterCategoryEntries(value: string): void {
    this.realCategoryEntries.length = 0;

    if (value) {
      switch (this.dropdownSelection) {
        case 'index':
          if (value.includes("-")) {
            let startAndEnd = value.split('-');
            if (startAndEnd.length != 2) return;
            if (Number.parseInt(startAndEnd[1]) % 10 < this.cachedCategoryEntries.length) {
              for (let i = (Number.parseInt(startAndEnd[0]) % 10); i <= (Number.parseInt(startAndEnd[1]) % 10); i++) {
                this.realCategoryEntries.push(this.cachedCategoryEntries[i]);
              }
            }
          } else if (this.hasNumber(value)) {
            this.realCategoryEntries.push(this.cachedCategoryEntries[Number.parseInt(value) % 10]);
          }
          break;
      }
    } else this.realCategoryEntries = [... this.cachedCategoryEntries];

  }

  getNextEntries(): void {
    this.currentPage++;
    this.initData(this.mode, this.currentPage);
  }

  getPreviousEntries(): void {
    this.currentPage--;
    this.initData(this.mode, this.currentPage);
  }

  getCardRelatedInfo(infoObject: any): void {
    let dialogTitle = this.capitalizeFLetter(infoObject.title) + " for ";
    dialogTitle += this.selectedEntry.name ? this.selectedEntry.name : this.selectedEntry.title;

    const dialogRef = this.matDialog.open(DialogOverviewExampleDialog, {
      width: '500px',
      data: {
        cardInfoTitle: dialogTitle,
        cardInfoData: infoObject.value
      }
    });
  }

  private capitalizeFLetter(word: string): any {
    return word[0].toUpperCase() +
      word.slice(1);
  }

  private humanize(str): void {
    let frags = str.split('_');
    for (let i = 0; i < frags.length; i++) {
      frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }
    return frags.join(' ');
  }

  private populatePeopleCard(key: any, value: any): void {
    if (key != "species" && key != "vehicles" && key != "starships" && key != "films") {
      this.cardFields.push({
        title: this.humanize(key),
        value: key === 'edited' || key === 'created' ? new Date(value).toDateString() : value
      });
    } else {
      this.cardButtonsData.push({
        title: key,
        value: value
      });
    }
  }

  private populatePlanetsCard(key: any, value: any): void {
    if (key != "residents" && key != "films") {
      this.cardFields.push({
        title: this.humanize(key),
        value: key === 'edited' || key === 'created' ? new Date(value).toDateString() : value
      });
    } else {
      this.cardButtonsData.push({
        title: key,
        value: value
      });
    }
  }

  private populateFilmsCard(key: any, value: any): void {
    if (key != "characters" && key != "species" && key != "vehicles" && key != "starships" && key != "planets") {
      this.cardFields.push({
        title: this.humanize(key),
        value: key === 'edited' || key === 'created' ? new Date(value).toDateString() : value
      });
    } else {
      this.cardButtonsData.push({
        title: key,
        value: value
      });
    }
  }

  private populateSpeciesCard(key: any, value: any): void {
    if (key != "films" && key != "people") {
      this.cardFields.push({
        title: this.humanize(key),
        value: key === 'edited' || key === 'created' ? new Date(value).toDateString() : value
      });
    } else {
      this.cardButtonsData.push({
        title: key,
        value: value
      });
    }
  }

  private populateVehiclesCard(key: any, value: any): void {
    if (key != "films" && key != "pilots") {
      this.cardFields.push({
        title: this.humanize(key),
        value: key === 'edited' || key === 'created' ? new Date(value).toDateString() : value
      });
    } else {
      this.cardButtonsData.push({
        title: key,
        value: value
      });
    }
  }

  private populateStarshipsCard(key: any, value: any): void {
    if (key != "films" && key != "pilots") {
      this.cardFields.push({
        title: this.humanize(key),
        value: key === 'edited' || key === 'created' ? new Date(value).toDateString() : value
      });
    } else {
      this.cardButtonsData.push({
        title: key,
        value: value
      });
    }
  }

  private hasNumber(myString) {
    return /\d/.test(myString);
  }

  private populateInitialSearchInput(length: number): void {

    const cachedIndex = this.modeService.getIndex();
    if (cachedIndex) {
      this.searchInput = cachedIndex;
      this.filterCategoryEntries((Number.parseInt(cachedIndex) % 10).toString());
      return;
    }

    if (length == 10) {
      this.searchInput = (this.currentPage - 1) * 10 + "-" + (this.currentPage * 10 - 1);
      this.isLastPage = false;
    } else {
      this.searchInput = (this.currentPage - 1) * 10 + "-" + ((this.currentPage - 1) * 10 + length - 1);
      this.isLastPage = true;
    }

    this.filterCategoryEntries(this.searchInput);

  }

}


@Component({
  selector: 'dialog-overview-example-dialog',
  template: `
    <h1 mat-dialog-title>{{data.cardInfoTitle}}</h1>
      <div mat-dialog-content class="fancy-scroll v-group" style="height: 250px;">
        <div *ngFor="let entry of data.cardInfoData" class="list-entry-class" (click)="switchModes(entry)">
          {{entry}}
        </div>
      </div>
    <div mat-dialog-actions class="d-f-row j-c-cc">
      <button mat-button (click)="closeDialog()" color="warn">Close</button>
    </div>
  `,
  styles: [`
    .list-entry-class {
        cursor: pointer;
        padding: 5px;
        border-radius: 10px;
        transition: 0.3s;
    }

    .list-entry-class:hover{
      background-color: #e0e0e0;
    }
  `]
})

export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    protected modeService: ModeService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  closeDialog(): void {
    this.dialogRef.close();
  }

  switchModes(entry: string): void {

    if (entry.includes('people')) {
      this.modeService.updateModeTwice('people');
    } else if (entry.includes('planets')) {
      this.modeService.updateModeTwice('planets');
    } else if (entry.includes('films')) {
      this.modeService.updateModeTwice('films');
    } else if (entry.includes('species')) {
      this.modeService.updateModeTwice('species');
    } else if (entry.includes('vehicles')) {
      this.modeService.updateModeTwice('vehicles');
    } else if (entry.includes('starships')) {
      this.modeService.updateModeTwice('starships');
    }

    const indexToBeSearched = this.getIndex(entry);
    this.modeService.setIndex(indexToBeSearched)
    this.closeDialog();
  }

  private getIndex(url: string): any {
    let urlArray = url.split("/");
    return urlArray[urlArray.length - 2];
  }

}