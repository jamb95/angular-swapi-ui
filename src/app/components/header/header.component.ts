import { Component, OnInit } from '@angular/core';
import { ModeService } from 'src/app/services/mode/mode.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

  constructor(
    protected modeService: ModeService
  ) { }

  ngOnInit(): void { }

  switchToDefaultMode(): void {
    this.modeService.updateMode('home');
    this.modeService.setIndex(null);
  }

}
