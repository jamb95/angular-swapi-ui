import { Component, OnInit } from '@angular/core';
import { ModeService } from 'src/app/services/mode/mode.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {

  constructor(
    protected modeService: ModeService
  ) { }

  ngOnInit(): void {
  }

  switchMode(mode: string): void {
    setTimeout(() => this.modeService.updateMode(mode), 500);
  }

}
