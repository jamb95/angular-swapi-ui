export interface MatDropdown {
    value: string;
    viewValue: string;
}
